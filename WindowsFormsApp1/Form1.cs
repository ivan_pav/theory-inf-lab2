﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public const string alphabet = "abcdefghijklmnopqrstuvwxyz";

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Вычисляет частоту входимости каждого символа.
        /// Возвращает словарь, сортированный по убыванию.
        /// </summary>
        public Dictionary<char, float> occurrenceCounting(string data)
        {
            string symbols = "";
            var letters = new Dictionary<char, float>();

            for (int indexSource = 0; indexSource < data.Length; indexSource++)
            {
                int symbolCounter = 0;
                if (!symbols.Contains(data[indexSource]) && alphabet.Contains(data[indexSource]))
                {
                    symbols += data[indexSource];
                    for (int indexIn = indexSource; indexIn < data.Length; indexIn++)
                    {
                        if (data[indexIn] == data[indexSource])
                        {
                            symbolCounter++;
                        }
                    }
                    letters.Add(data[indexSource], (float)symbolCounter / data.Length * 100);
                }
            }
            letters = letters.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            return letters;
        }

        /// <summary>
        /// Создаёт словарь, ключом которого является символ из зашифроманного текста,
        /// а значением - символ, на который его нужно заменить.
        /// </summary>
        public Dictionary<char, char> creatingDictionaryToReplace(Dictionary<char, float> letters)
        {
            string dataFromTheTable = "etaoinshrdlcumwfgypbvkxjqz";

            var change = new Dictionary<char, char>();
            int indexCounter = 0;
            foreach (var item in letters)
            {
                change.Add(item.Key, dataFromTheTable[indexCounter]);
                indexCounter++;
            }
            return change;
        }

        /// <summary>
        /// Расшифровывает текст
        /// </summary>
        public string decode(string data, Dictionary<char, char> change)
        {
            char[] result = data.ToArray();

            for (int index = 0; index < result.Length; index++)
            {
                if (alphabet.Contains(result[index]))
                {
                    result[index] = change[result[index]];
                }
            }

            string str = new string(result);
            return str;
        }

        /// <summary>
        /// Кнопка "Взломать"
        /// Меняет символы в зашифроманном тексте на соответствующие им по частоте встречаемости
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            textBox3.Clear();

            string sourceData = textBox4.Text;
            var letters = occurrenceCounting(sourceData);
            var change = creatingDictionaryToReplace(letters);
            string info;
            foreach (var item in letters)
            {
                info = item.Key + $" - " + String.Format("{0:F5}", item.Value) + "%";
                textBox3.Text += info + Environment.NewLine;
            }

            textBox2.Text = decode(sourceData, change);
        }

        /// <summary>
        /// Кнопка "Заменить"
        /// Меняет буквы во взломанном тексте.
        /// Например если ввести 'p' и 'q', то все 'p' станут 'q', а все 'q' станут 'p'
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            char[] data = textBox2.Text.ToArray();
            char firstSymbol = Convert.ToChar(textBox5.Text);
            char futureSymbol = Convert.ToChar(textBox6.Text);
            for (int index = 0; index < data.Length; index++)
            {
                if (data[index] == firstSymbol)
                {
                    data[index] = futureSymbol;
                }
                else if(data[index] == futureSymbol)
                {
                    data[index] = firstSymbol;
                }
            }
            textBox2.Text = new string(data);
        }

        /// <summary>
        /// Кнопка "Зашифровать"
        /// Шифрует текст по формуле из первой лабораторной работы
        /// E(k) = i * k (mod m)
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            char[] data = textBox1.Text.ToLower().ToArray();
            int key = 3;
            int power = 26;
            for (int index = 0; index < data.Length; index++)
            {
                if (alphabet.Contains(data[index]))
                {
                    data[index] = alphabet[alphabet.IndexOf(data[index]) * key % power];
                }
            }
            textBox4.Text = new string(data);
        }
    }
}
